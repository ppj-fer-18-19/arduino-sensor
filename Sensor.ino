/*
 * Arduino senzor i YUN
 */



//Konstante koje odreÄuju pinove na koje se spaja ultrazvucni senzor
const int trigPin = 10; //Input pin
const int echoPin = 11; //Output pin
const int led = 13;
//Interval nakon kojeg se osvjezava rezultat za parkirno mjesto 
const long interval =  300;

const int udaljenostSlobodno = 200;

//izmjerena udaljenost je u cm ,a trajanje putovanja zvuka u uS

  void setup() 
  {
  pinMode(led,OUTPUT);
  pinMode(trigPin, OUTPUT); 
  pinMode(echoPin, INPUT);
  Serial.begin(9600);    
  }


void loop() 
{
long trajanjePutovanjaZvuka = 0;
long izmjerenaUdaljenost = 0;

// Resetiranje triggera 
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
// Kako bi se emitirao zvuk potrebno je postaviti trigger na 1 na 10uS
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10); 
  digitalWrite(trigPin, LOW);
  

// Vraca vrijeme povratka zvuka u uS
  trajanjePutovanjaZvuka = pulseIn(echoPin, HIGH);
 
  izmjerenaUdaljenost = trajanjePutovanjaZvuka*0.034/2;
  Serial.println(izmjerenaUdaljenost);
  Serial.println(trajanjePutovanjaZvuka);

  //Ako je mjesto izmjerena udaljenost manja od konstante slobodnog mjesta onda je mjesto zauzeto
  if(izmjerenaUdaljenost < udaljenostSlobodno)   
  {
    digitalWrite(led,HIGH);


  //slanje informacije da je mjesto zauzeto na server
    
  }else{
    digitalWrite(led,LOW);
  //slanje informacije da je mjesto slobodno
    
  }
  delay(100);

}
